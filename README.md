## Requirements
We will work with the ROS Kinetic Kame distribution which is supported with Ubuntu Wily/Xenial or Debian Jessie. You can either install it manually on your computer or&mdash;_recommended_&mdash;use a virtual machine with our provided image (which includes also [Sublime Text 3](https://www.sublimetext.com/3 "Sublime Text's Homepage") and [Terminator](https://gnometerminator.blogspot.com/ "Terminator's Homepage")).

## How to get started with the VM
1. You can download the image from the university's [DataSync page](https://datasync.ed.ac.uk/index.php/s/hWtj6aytoZu8joA "VB image") (&asymp; 3.2GB)
2. Install the [VirtualBox](https://www.virtualbox.org/ "VirtualBox's Homepage") binary corresponding to your OS

    **Windows** &amp; **MacOS**: Grab and install the binaries directly from the [Downloads page](https://www.virtualbox.org/wiki/Downloads "VirtualBox's Downloads page")

    **Ubuntu**: You can use the package manager: `sudo apt install virtualbox`

3. Start the VirtualBox app and click `File`&rarr;`Import Appliance... `
4. Choose the `Introduction to ROS.ova` included in the repo and click `Next >`
5. On the next page check the settings. Make sure that the RAM configuration doesn't exceed half of your machine's specifications
6. Select the `Introduction to ROS` VM from the main page of the VirtualBox app and click `Start`
7. Once the VM boots, a terminal will automatically start. Type `roscore` to check whether everything went as expected. You should get a message ending with

    > started core service [/rosout]

8. Hit `Ctrl+C` in order to kill it
9. Congratulations, you have a ROS-ready machine :v: